@extends(config('status.uiLayout', 'MiamiTheme::layouts.noNav'))

@section('content')
    <h2>You are not authorized to access the status page.</h2>
@endsection

@section('javascript')

@endsection

@section('css')

@endsection
