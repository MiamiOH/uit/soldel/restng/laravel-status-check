@extends(config('status.uiLayout', 'MiamiTheme::layouts.noNav'))

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h2>Application Information</h2>
        </div>
        <div class="col-xs-12">
            @foreach($attributes as $attribute)
                <div class="row">
                    <div class="col-lg-4 col-md-5 col-sm-8 col-xs-8 status-name">
                        {{ $attribute['description'] }}
                    </div>
                    <div class="col-lg-8 col-md-7 col-sm-4 col-xs-4 status-name">
                        {{ $attribute['value'] }}
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <h2>Application Status
                <button class="btn btn-alt-brand btn-sm"
                        style="display: none; line-height: 0.75rem;"
                        id="forceRefreshBtn">Force Refresh
                </button>
            </h2>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12" id="applicationStatusLoading">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
        <div class="col-xs-12" id="statusItemContainer"></div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <h2>API Endpoint</h2>
        </div>
        <div class="col-xs-12">
            <p><strong>Endpoint:</strong> {{ route('check-status') }}&nbsp;&nbsp;<a class="btn btn-alt-brand btn-sm" style="line-height: 0.5rem;" href="{{ route('check-status') }}" target="_blank">&nbsp;Try it&nbsp;</a></p>
            <p style="opacity: 0.75">Note: To bypass cache, add query parameter <strong>force-refresh=true</strong>: <a href="{{ route('check-status', ['force-refresh' => 'true']) }}" target="_blank">link</a>.</p>
        </div>
    </div>
@endsection

@section('javascript')
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script>
        const getRequests = () => {
            return axios.get('{{ route('status-get-requests') }}')
                .then((res) => {
                    return res['data'];
                });
        };

        const retrieveStatus = (req, forceRefresh = false) => {
            const params = {
                request: req
            };

            if (forceRefresh) {
                params['force-refresh'] = 'true';
            }

            return axios.get('{{ route('check-status') }}', {params: params})
                .then((res) => {
                    return res['data'];
                });
        };

        const createCheckerItem = (req) => {
            return new Promise((resolve, reject) => {
                const row = document.createElement('div');
                row.className = 'row checker-item';

                const nameDiv = document.createElement('div');
                nameDiv.className = 'col-lg-4 col-md-5 col-sm-8 col-xs-8';
                const nameSpan = document.createElement("span");
                nameSpan.className = 'status-name';
                nameSpan.innerText = req['name'];
                nameDiv.appendChild(nameSpan);

                const statusDiv = document.createElement('div');
                statusDiv.className = 'col-lg-8 col-md-7 col-sm-4 col-xs-4 status-text-container';
                statusDiv.innerHTML += '<i class="fa fa-refresh fa-spin"></i>';

                const hidden = document.createElement("span");
                hidden.className = 'status-request';
                hidden.innerText = JSON.stringify(req);

                row.appendChild(nameDiv);
                row.appendChild(statusDiv);
                row.appendChild(hidden);

                const statusItemContainer = document.getElementById('statusItemContainer');
                statusItemContainer.appendChild(row);

                resolve(statusDiv);
            });
        };

        const displayStatus = (data, statusDiv) => {
            const span = document.createElement('span');

            if (data['isOk']) {
                span.className = 'status-success';
            } else {
                span.className = 'status-error';
            }

            let message = data['message'];

            if (data['comment']) {
                message += ': ' + data['comment'];
            }

            span.innerText = message;
            statusDiv.innerHTML = '';
            statusDiv.appendChild(span);
        };

        const forceRefreshStatus = () => {
            // const loading = document.createElement('i');
            // i.className = 'fa fa-refresh fa-spin';

            const forceRefreshBtn = document.getElementById('forceRefreshBtn');
            forceRefreshBtn.innerHTML += '&nbsp;&nbsp;<i class="fa fa-refresh fa-spin"></i>';
            forceRefreshBtn.disabled = true;

            const checkerItems = document.getElementsByClassName('checker-item');
            let numOfRemaining = checkerItems.length;
            for (const item of checkerItems) {
                const request = item.getElementsByClassName('status-request')[0];
                const statusDiv = item.getElementsByClassName('status-text-container')[0];

                statusDiv.innerHTML = '<i class="fa fa-refresh fa-spin"></i>';
                retrieveStatus(request.innerText, true)
                    .then((data) => displayStatus(data, statusDiv))
                    .then(() => {
                        numOfRemaining--;
                        if (numOfRemaining <= 0) {
                            document.getElementById('forceRefreshBtn').disabled = false;
                            forceRefreshBtn.innerText = 'Force Refresh';
                        }
                    });
            }
        };

        document.addEventListener("DOMContentLoaded", () => {
            getRequests().then((requests) => {
                document.getElementById('applicationStatusLoading').style.display = 'none';
                let numOfRemaining = requests.length;

                for (const request of requests) {
                    createCheckerItem(request).then((statusDiv) => {
                        retrieveStatus(JSON.stringify(request))
                            .then((data) => displayStatus(data, statusDiv))
                            .then(() => {
                                numOfRemaining--;
                                if (numOfRemaining <= 0) {
                                    document.getElementById('forceRefreshBtn').style.display = 'inline-block';
                                }
                            });
                    });
                }
            });

            document.getElementById('forceRefreshBtn').addEventListener('click', forceRefreshStatus);
        });
    </script>
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        h2 {
            font-size: 2rem;
        }

        .status-success {
            font-size: 1.5rem;
            color: #3dc954;
        }

        .status-error {
            font-size: 1.5rem;
            color: #ff5252;
        }

        .status-name {
            font-size: 1.5rem;
        }

        .status-request {
            display: none;
        }

        i.fa {
            color: #c3142d;
        }

        .btn-alt-brand:hover > i {
            color: white;
        }
    </style>
@endsection
