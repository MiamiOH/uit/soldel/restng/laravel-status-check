<?php

return [
    /***************************************************************************
     *
     * The status page uses layout from laravel-miami-theme package.
     * It should match with the layout used in your application.
     *
     * Available Layouts:
     * https://git.itapps.miamioh.edu/it/soldel/laravel-miami-theme/-/tree/master/src%2FMiamiOH%2FLaravelMiamiTheme%2Fviews%2Flayouts
     *
     ***************************************************************************/
    'uiLayout' => 'MiamiTheme::layouts.noNav',

    /***************************************************************************
     *
     * Status is put in the cache once requested. Define the cache
     * timeout in the below (in seconds).
     * If set to 0, status will not be put in the cache.
     *
     ***************************************************************************/
    'cacheTimeout' => 60,

    /***************************************************************************
     *
     * Control whether user can access the status page
     *
     * Available Values:
     *
     * 1. 'anonymous': any user (without signed in) can access
     * 2. 'cas': any user logged in from CAS can access
     * 3. one or more AuthMan config, e.g.
     *     [
     *       'application' => 'LaravelStatusCheck',
     *       'category' => 'Web',
     *       'key' => 'full',
     *     ]
     *
     ***************************************************************************/
    'authorization' => [
        'anonymous',
    ],

    /***************************************************************************
     *
     * add DB connections used in your application, e.g. mysql, mariadb, etc.
     * Only PDO compatible DB connections are supported.
     * (other connections are not supported, e.g. redis, mangodb, etc )
     *
     * Syntax:
     * [
     *     '<db_connection_name>' => [
     *         'checkMigrations' => true/false
     *     ]
     * ]
     *
     * if "checkMigrations" set to true, them table migration will be check by command:
     *
     *   php artisan migrate:status --database=<db_connection_name>
     *
     ***************************************************************************/
    'db' => [
        'oracle' => [
            'checkMigrations' => false,
        ],
        'configManager' => [
            'checkMigrations' => false,
        ],
    ],

    /***************************************************************************
     *
     * Check RESTng Status (use laravel-restng-integration)
     *
     * 1. 'checkReachable': check whether RESTng server is reachable.
     *      if set to false, 'verifyCredentials` and `verifyAuthorizations`
     *      are ignored.
     *
     * 2. 'verifyCredentials': verify username/password or tokenString by
     *      invoking RESTng Authentication.
     *    Available Value: credential names from config/restng.php:
     *      credentials.token.*
     *
     * 3. 'verifyAuthorizations': verify whether the user is authorized to
     *      the following resource.
     *     e.g.
     *       [
     *           'application' => 'WebServices',
     *           'category' => 'AccountReceivable',
     *           'key' => 'postTransaction',
     *           'credentialName' => 'default' // credential names from config/restng.php: credentials.token.*
     *       ]
     *
     * 4. 'checkResources': check whether resources are deployed.
     *       e.g.
     *          random.string
     *          authentication.v1.create
     *
     ***************************************************************************/
    'restng' => [
        'checkReachable' => true,

        'verifyCredentials' => [
            'default',
        ],

        'verifyAuthorizations' => [
            //
        ],

        'checkResources' => [
            //
        ],
    ],

    /***************************************************************************
     *
     * Custom Checker
     *
     * (required) write a class that implement interface
     *            (must register in the ServiceProvider):
     *              \MiamiOH\LaravelStatusCheck\Checkers\StatusChecker
     *
     * (optional) write a class that implement interface
     *            (must register in the ServiceProvider):
     *              \MiamiOH\LaravelStatusCheck\Resolvers\ParamResolver
     *
     * Config Example:
     *
     * <status_checker_class::class> => [
     *     // one or more parameters
     * ]
     *
     * OR
     *
     * <status_checker_class::class> => <param_resolver::class>
     *
     ***************************************************************************/
    'custom' => [
        //
    ],

    /***************************************************************************
     *
     * Get a list of attribute and display on the status page
     *
     * Example 1: an exact value
     *
     *    'environment' => [
     *        'description' => 'Environment',
     *        'value' => env('APP_ENV'),
     *    ],
     *
     * Example 2: define a class that implement interface:
     *    (must register in the ServiceProvider)
     *    \MiamiOH\LaravelStatusCheck\Resolvers\ValueResolver
     *
     *    'key' => [
     *        'description' => 'something ...',
     *        'resolver' => <value_resolver::class> // value is resolved at runtime
     *    ]
     *
     ***************************************************************************/
    'attributes' => [
        'environment' => [
            'description' => 'Environment',
            'value' => env('APP_ENV'),
        ],
        'laravel_version' => [
            'description' => 'Laravel Version',
            'value' => \Illuminate\Foundation\Application::VERSION,
        ],
    ],
];