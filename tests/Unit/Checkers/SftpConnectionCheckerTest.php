<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 5/10/20
 * Time: 10:41 PM
 */

namespace MiamiOH\LaravelStatusCheck\Test\Unit\Checkers;


use Illuminate\Contracts\Filesystem\Factory;
use Illuminate\Contracts\Filesystem\Filesystem;
use MiamiOH\LaravelStatusCheck\Checkers\SftpConnectionChecker;
use MiamiOH\LaravelStatusCheck\Test\Unit\TestCase;
use PHPUnit\Framework\MockObject\MockObject;

/** @covers \MiamiOH\LaravelStatusCheck\Checkers\SftpConnectionChecker */
class SftpConnectionCheckerTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $fsFactory;
    /**
     * @var SftpConnectionChecker
     */
    private $checker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->fsFactory = $this->createMock(Factory::class);
        $this->checker = new SftpConnectionChecker($this->fsFactory);
    }

    public function testGetName()
    {
        $this->assertSame('SFTP Connection [disk1]', $this->checker->getName([
            'name' => 'disk1',
        ]));
    }

    public function testSftpConnectionIsConnected()
    {
        $fs = $this->getMockBuilder(Filesystem::class)
            ->disableAutoload()
            ->setMethods([
                'getDriver',
                'getAdapter',
                'connect',
            ])
            ->getMock();
        $fs->expects($this->once())->method('getDriver')->willReturnSelf();
        $fs->expects($this->once())->method('getAdapter')->willReturnSelf();
        $fs->expects($this->once())->method('connect');

        $this->fsFactory->expects($this->once())
            ->method('disk')
            ->with($this->equalTo('disk1'))
            ->willReturn($fs);

        $status = $this->checker->check(['name' => 'disk1']);
        $this->assertSame([
            'name' => 'SFTP Connection [disk1]',
            'isOk' => true,
            'message' => 'Connected',
            'comment' => null,
        ], $status->toJsonArray());
    }

    public function testSftpConnectionIsNotConnected()
    {
        $fs = $this->getMockBuilder(Filesystem::class)
            ->disableAutoload()
            ->setMethods([
                'getDriver',
                'getAdapter',
                'connect',
            ])
            ->getMock();
        $fs->expects($this->once())->method('getDriver')
            ->willThrowException(new \Exception('abcdef'));

        $this->fsFactory->expects($this->once())
            ->method('disk')
            ->with($this->equalTo('disk1'))
            ->willReturn($fs);

        $status = $this->checker->check(['name' => 'disk1']);
        $this->assertSame([
            'name' => 'SFTP Connection [disk1]',
            'isOk' => false,
            'message' => 'Not Connected',
            'comment' => 'abcdef',
        ], $status->toJsonArray());
    }
}
