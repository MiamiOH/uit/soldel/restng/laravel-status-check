<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 5/10/20
 * Time: 10:41 PM
 */

namespace MiamiOH\LaravelStatusCheck\Test\Unit\Checkers;


use Illuminate\Contracts\Console\Kernel;
use Illuminate\Database\DatabaseManager;
use MiamiOH\LaravelStatusCheck\Checkers\Database\DatabaseMigrationChecker;
use MiamiOH\LaravelStatusCheck\Test\Unit\TestCase;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\Console\Output\BufferedOutput;

/**
 * @covers \MiamiOH\LaravelStatusCheck\Checkers\Database\DatabaseMigrationChecker
 * @covers \MiamiOH\LaravelStatusCheck\Checkers\Database\BaseDatabaseChecker
 */
class DatabaseMigrationCheckerTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $dbFactory;
    /**
     * @var MockObject
     */
    private $console;
    /**
     * @var DatabaseMigrationChecker
     */
    private $checker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->dbFactory = $this->createMock(DatabaseManager::class);
        $this->console = $this->createMock(Kernel::class);
        $this->checker = new DatabaseMigrationChecker(
            $this->dbFactory,
            $this->console
        );
    }

    public function testGetName()
    {
        $this->assertSame('Database Migration [oracle]', $this->checker->getName([
            'name' => 'oracle',
        ]));
    }

    public function testMigrationIsDoneIfAllTablesAreMigrated()
    {
        $this->console->method('call')
            ->willReturnCallback(function ($cmd, $options, BufferedOutput $buffer) {
                $buffer->write(<<<EOD
+------+--------------------------------------------+-------+
| Ran? | Migration                                  | Batch |
+------+--------------------------------------------+-------+
| Yes  | 2020_03_18_112200_create_sources_table     | 1     |
| Yes  | 2020_03_18_112720_create_files_table       | 1     |
| Yes  | 2020_03_18_114437_create_results_table     | 1     |
| Yes  | 2020_04_10_123103_create_jobs_table        | 1     |
| Yes  | 2020_04_12_223852_create_failed_jobs_table | 1     |
+------+--------------------------------------------+-------+
EOD
                );
            });

        $status = $this->checker->check(['name' => 'oracle']);
        $this->assertSame([
            'name' => 'Database Migration [oracle]',
            'isOk' => true,
            'message' => 'Migrated',
            'comment' => null,
        ], $status->toJsonArray());
    }

    public function testMigrationIsNotDoneIfAnyTablesAreNotMigrated()
    {
        $output = <<<EOD
+------+--------------------------------------------+-------+
| Ran? | Migration                                  | Batch |
+------+--------------------------------------------+-------+
| Yes  | 2020_03_18_112200_create_sources_table     | 1     |
| Yes  | 2020_03_18_112720_create_files_table       | 1     |
| No  | 2020_03_18_114437_create_results_table     | 1     |
| Yes  | 2020_04_10_123103_create_jobs_table        | 1     |
| Yes  | 2020_04_12_223852_create_failed_jobs_table | 1     |
+------+--------------------------------------------+-------+
EOD;
        $this->console->method('call')
            ->willReturnCallback(function ($cmd, $options, BufferedOutput $buffer) use ($output) {
                $buffer->write($output);
            });

        $status = $this->checker->check(['name' => 'oracle']);
        $this->assertSame([
            'name' => 'Database Migration [oracle]',
            'isOk' => false,
            'message' => 'Not Migrated',
            'comment' => $output,
        ], $status->toJsonArray());
    }

    public function testMigrationIsNotDoneIfThereIsNoMigrationFile()
    {
        $this->console->method('call')
            ->willReturnCallback(function ($cmd, $options, BufferedOutput $buffer) {
                $buffer->write('No migrations found.');
            });

        $status = $this->checker->check(['name' => 'oracle']);
        $this->assertSame([
            'name' => 'Database Migration [oracle]',
            'isOk' => false,
            'message' => 'No migrations found',
            'comment' => null,
        ], $status->toJsonArray());
    }
}
