<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 5/10/20
 * Time: 10:41 PM
 */

namespace MiamiOH\LaravelStatusCheck\Test\Unit\Checkers;


use Illuminate\Contracts\Console\Kernel;
use Illuminate\Database\Connection;
use Illuminate\Database\DatabaseManager;
use MiamiOH\LaravelStatusCheck\Checkers\Database\DatabaseConnectionChecker;
use MiamiOH\LaravelStatusCheck\Test\Unit\TestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @covers \MiamiOH\LaravelStatusCheck\Checkers\Database\DatabaseConnectionChecker
 * @covers \MiamiOH\LaravelStatusCheck\Checkers\Database\BaseDatabaseChecker
 */
class DatabaseConnectionCheckerTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $dbFactory;
    /**
     * @var MockObject
     */
    private $console;
    /**
     * @var DatabaseConnectionChecker
     */
    private $checker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->dbFactory = $this->createMock(DatabaseManager::class);
        $this->console = $this->createMock(Kernel::class);
        $this->checker = new DatabaseConnectionChecker(
            $this->dbFactory,
            $this->console
        );
    }

    public function testGetName()
    {
        $this->assertSame('Database Connection [oracle]', $this->checker->getName([
            'name' => 'oracle',
        ]));
    }

    public function testDatabaseConnectionIsConnected()
    {
        $connection = $this->createMock(Connection::class);
        $connection->expects($this->once())->method('getPdo');

        $this->dbFactory->method('connection')
            ->with($this->equalTo('oracle'))
            ->willReturn($connection);

        $status = $this->checker->check(['name' => 'oracle']);
        $this->assertSame([
            'name' => 'Database Connection [oracle]',
            'isOk' => true,
            'message' => 'Connected',
            'comment' => null,
        ], $status->toJsonArray());
    }
}
