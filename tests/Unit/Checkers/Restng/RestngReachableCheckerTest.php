<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 5/10/20
 * Time: 11:34 PM
 */

namespace MiamiOH\LaravelStatusCheck\Test\Unit\Checkers\Restng;


use MiamiOH\LaravelRestng\Laravel\CredentialPool;
use MiamiOH\LaravelRestng\Responses\RestngResponse;
use MiamiOH\LaravelRestng\RestngClient;
use MiamiOH\LaravelStatusCheck\Checkers\Restng\RestngReachableChecker;
use MiamiOH\LaravelStatusCheck\Test\Unit\TestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @covers \MiamiOH\LaravelStatusCheck\Checkers\Restng\RestngReachableChecker
 * @covers \MiamiOH\LaravelStatusCheck\Checkers\Restng\BaseRestngChecker
 */
class RestngReachableCheckerTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $restngClient;
    /**
     * @var MockObject
     */
    private $credentialPool;
    /**
     * @var RestngReachableChecker
     */
    private $checker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->restngClient = $this->createMock(RestngClient::class);
        $this->credentialPool = $this->createMock(CredentialPool::class);
        $this->checker = new RestngReachableChecker(
            $this->restngClient,
            $this->credentialPool
        );
    }

    public function testGetName()
    {
        $this->restngClient->method('getRestngUrl')
            ->willReturn('https://api.ws');

        $this->assertSame('RESTng: https://api.ws', $this->checker->getName());
    }

    public function testRestngIsReachable()
    {
        $this->restngClient->method('getRestngUrl')
            ->willReturn('https://api.ws');

        $this->restngClient->method('get')
            ->with($this->equalTo('/api'))
            ->willReturn(new RestngResponse([], 200));

        $status = $this->checker->check();
        $this->assertSame([
            'name' => 'RESTng: https://api.ws',
            'isOk' => true,
            'message' => 'Operational',
            'comment' => null,
        ], $status->toJsonArray());
    }

    public function testRestngIsNotReachable()
    {
        $this->restngClient->method('getRestngUrl')
            ->willReturn('https://api.ws');

        $this->restngClient->method('get')
            ->with($this->equalTo('/api'))
            ->willReturn(new RestngResponse([], 500));

        $status = $this->checker->check();
        $this->assertSame([
            'name' => 'RESTng: https://api.ws',
            'isOk' => false,
            'message' => 'Not Reachable',
            'comment' => null,
        ], $status->toJsonArray());
    }
}