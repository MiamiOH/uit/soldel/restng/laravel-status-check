<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 5/10/20
 * Time: 11:34 PM
 */

namespace MiamiOH\LaravelStatusCheck\Test\Unit\Checkers\Restng;


use MiamiOH\LaravelRestng\Auth\Authenticatable;
use MiamiOH\LaravelRestng\Auth\Token\UsernamePassword;
use MiamiOH\LaravelRestng\Laravel\CredentialPool;
use MiamiOH\LaravelRestng\RestngClient;
use MiamiOH\LaravelStatusCheck\Checkers\Restng\RestngCredentialChecker;
use MiamiOH\LaravelStatusCheck\Test\Unit\TestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @covers \MiamiOH\LaravelStatusCheck\Checkers\Restng\RestngCredentialChecker
 * @covers \MiamiOH\LaravelStatusCheck\Checkers\Restng\BaseRestngChecker
 */
class RestngCredentialCheckerTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $restngClient;
    /**
     * @var MockObject
     */
    private $credentialPool;
    /**
     * @var RestngCredentialChecker
     */
    private $checker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->restngClient = $this->createMock(RestngClient::class);
        $this->credentialPool = $this->createMock(CredentialPool::class);
        $this->checker = new RestngCredentialChecker(
            $this->restngClient,
            $this->credentialPool
        );
    }

    public function testGetName()
    {
        $this->credentialPool->method('get')
            ->with($this->equalTo('default'))
            ->willReturn(new UsernamePassword('user1', 'abcd'));

        $this->assertSame('RESTng Authentication: user1', $this->checker->getName([
            'name' => 'default',
        ]));
    }

    public function testRestngCredentialIsValid()
    {
        $credential = $this->createMock(Authenticatable::class);
        $credential->method('isValid')
            ->with($this->equalTo($this->restngClient))
            ->willReturn(true);
        $credential->method('getUsername')->willReturn('user1');

        $this->credentialPool->method('get')
            ->with($this->equalTo('default'))
            ->willReturn($credential);

        $status = $this->checker->check([
            'name' => 'default',
        ]);
        $this->assertSame([
            'name' => 'RESTng Authentication: user1',
            'isOk' => true,
            'message' => 'Authenticated',
            'comment' => null,
        ], $status->toJsonArray());
    }

    public function testUsernamePasswordNotMatch()
    {
        $credential = $this->createMock(Authenticatable::class);
        $credential->method('isValid')
            ->with($this->equalTo($this->restngClient))
            ->willReturn(false);
        $credential->method('getUsername')->willReturn('user1');

        $this->credentialPool->method('get')
            ->with($this->equalTo('default'))
            ->willReturn($credential);

        $status = $this->checker->check([
            'name' => 'default',
        ]);
        $this->assertSame([
            'name' => 'RESTng Authentication: user1',
            'isOk' => false,
            'message' => 'Not Authenticated: invalid username/password',
            'comment' => null,
        ], $status->toJsonArray());
    }
}