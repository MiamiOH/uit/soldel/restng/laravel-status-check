<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 5/10/20
 * Time: 11:34 PM
 */

namespace MiamiOH\LaravelStatusCheck\Test\Unit\Checkers\Restng;


use MiamiOH\LaravelRestng\Laravel\CredentialPool;
use MiamiOH\LaravelRestng\Responses\RestngResponse;
use MiamiOH\LaravelRestng\RestngClient;
use MiamiOH\LaravelStatusCheck\Checkers\Restng\RestngResourceChecker;
use MiamiOH\LaravelStatusCheck\Test\Unit\TestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @covers \MiamiOH\LaravelStatusCheck\Checkers\Restng\RestngResourceChecker
 * @covers \MiamiOH\LaravelStatusCheck\Checkers\Restng\BaseRestngChecker
 */
class RestngResourceCheckerTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $restngClient;
    /**
     * @var MockObject
     */
    private $credentialPool;
    /**
     * @var RestngResourceChecker
     */
    private $checker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->restngClient = $this->createMock(RestngClient::class);
        $this->credentialPool = $this->createMock(CredentialPool::class);
        $this->checker = new RestngResourceChecker(
            $this->restngClient,
            $this->credentialPool
        );
    }

    public function testGetName()
    {
        $this->assertSame('RESTng Resource: auth.v1', $this->checker->getName([
            'name' => 'auth.v1',
        ]));
    }

    public function testRestngResourceIsNotConfigured()
    {
        $this->restngClient->method('get')
            ->with($this->equalTo('/api/resource/auth.v1'))
            ->willReturn(new RestngResponse([], 404));

        $status = $this->checker->check(['name' => 'auth.v1']);
        $this->assertSame([
            'name' => 'RESTng Resource: auth.v1',
            'isOk' => false,
            'message' => 'Not Found',
            'comment' => null,
        ], $status->toJsonArray());
    }

    public function testRestngGETResourceIsConfigured()
    {
        $this->restngClient->method('get')
            ->with($this->equalTo('/api/resource/auth.v1'))
            ->willReturn(new RestngResponse([
                'action' => 'read',
                'pattern' => '/auth',
            ], 200));

        $status = $this->checker->check(['name' => 'auth.v1']);
        $this->assertSame([
            'name' => 'RESTng Resource: auth.v1',
            'isOk' => true,
            'message' => 'Configured',
            'comment' => 'GET /auth',
        ], $status->toJsonArray());
    }

    public function testRestngPOSTResourceIsConfigured()
    {
        $this->restngClient->method('get')
            ->with($this->equalTo('/api/resource/auth.v1'))
            ->willReturn(new RestngResponse([
                'action' => 'create',
                'pattern' => '/auth',
            ], 200));

        $status = $this->checker->check(['name' => 'auth.v1']);
        $this->assertSame([
            'name' => 'RESTng Resource: auth.v1',
            'isOk' => true,
            'message' => 'Configured',
            'comment' => 'POST /auth',
        ], $status->toJsonArray());
    }

    public function testRestngPUTResourceIsConfigured()
    {
        $this->restngClient->method('get')
            ->with($this->equalTo('/api/resource/auth.v1'))
            ->willReturn(new RestngResponse([
                'action' => 'update',
                'pattern' => '/auth',
            ], 200));

        $status = $this->checker->check(['name' => 'auth.v1']);
        $this->assertSame([
            'name' => 'RESTng Resource: auth.v1',
            'isOk' => true,
            'message' => 'Configured',
            'comment' => 'PUT /auth',
        ], $status->toJsonArray());
    }

    public function testRestngDELETEResourceIsConfigured()
    {
        $this->restngClient->method('get')
            ->with($this->equalTo('/api/resource/auth.v1'))
            ->willReturn(new RestngResponse([
                'action' => 'delete',
                'pattern' => '/auth',
            ], 200));

        $status = $this->checker->check(['name' => 'auth.v1']);
        $this->assertSame([
            'name' => 'RESTng Resource: auth.v1',
            'isOk' => true,
            'message' => 'Configured',
            'comment' => 'DELETE /auth',
        ], $status->toJsonArray());
    }
}