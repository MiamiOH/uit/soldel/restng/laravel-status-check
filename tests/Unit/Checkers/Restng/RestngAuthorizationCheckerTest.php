<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 5/10/20
 * Time: 11:34 PM
 */

namespace MiamiOH\LaravelStatusCheck\Test\Unit\Checkers\Restng;


use MiamiOH\LaravelRestng\Auth\Authenticatable;
use MiamiOH\LaravelRestng\Laravel\CredentialPool;
use MiamiOH\LaravelRestng\Responses\RestngResponse;
use MiamiOH\LaravelRestng\RestngClient;
use MiamiOH\LaravelStatusCheck\Checkers\Restng\RestngAuthorizationChecker;
use MiamiOH\LaravelStatusCheck\Test\Unit\TestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @covers \MiamiOH\LaravelStatusCheck\Checkers\Restng\RestngAuthorizationChecker
 * @covers \MiamiOH\LaravelStatusCheck\Checkers\Restng\BaseRestngChecker
 */
class RestngAuthorizationCheckerTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $restngClient;
    /**
     * @var MockObject
     */
    private $credentialPool;
    /**
     * @var RestngAuthorizationChecker
     */
    private $checker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->restngClient = $this->createMock(RestngClient::class);
        $this->credentialPool = $this->createMock(CredentialPool::class);
        $this->checker = new RestngAuthorizationChecker(
            $this->restngClient,
            $this->credentialPool
        );
    }

    public function testGetName()
    {
        $credential = $this->createMock(Authenticatable::class);
        $credential->method('getUsername')->willReturn('user1');

        $this->credentialPool->method('get')
            ->with($this->equalTo('default'))
            ->willReturn($credential);

        $this->assertSame(
            'RESTng Authorization (Application: app, Category: cat, Key: k) for User ("user1")',
            $this->checker->getName([
                'application' => 'app',
                'category' => 'cat',
                'key' => 'k',
                'credentialName' => 'default',
            ])
        );
    }

    public function testRestngUserIsAuthorizedToSpecifiedApp()
    {
        $credential = $this->createMock(Authenticatable::class);
        $credential->method('getUsername')->willReturn('user1');

        $this->credentialPool->method('get')
            ->with($this->equalTo('default'))
            ->willReturn($credential);

        $this->restngClient->method('get')
            ->with(
                $this->equalTo('/authorization/v1/app/cat/k'),
                $this->equalTo(['username' => 'user1'])
            )
            ->willReturn(new RestngResponse([
                'allowed' => 'true',
            ], 200));

        $status = $this->checker->check([
            'application' => 'app',
            'category' => 'cat',
            'key' => 'k',
            'credentialName' => 'default',
        ]);

        $this->assertSame([
            'name' => 'RESTng Authorization (Application: app, Category: cat, Key: k) for User ("user1")',
            'isOk' => true,
            'message' => 'Authorized',
            'comment' => null,
        ], $status->toJsonArray());
    }

    public function testRestngUserIsNotAuthorizedToSpecifiedApp()
    {
        $credential = $this->createMock(Authenticatable::class);
        $credential->method('getUsername')->willReturn('user1');

        $this->credentialPool->method('get')
            ->with($this->equalTo('default'))
            ->willReturn($credential);

        $this->restngClient->method('get')
            ->with(
                $this->equalTo('/authorization/v1/app/cat/k'),
                $this->equalTo(['username' => 'user1'])
            )
            ->willReturn(new RestngResponse([
                'allowed' => 'false',
            ], 200));

        $status = $this->checker->check([
            'application' => 'app',
            'category' => 'cat',
            'key' => 'k',
            'credentialName' => 'default',
        ]);

        $this->assertSame([
            'name' => 'RESTng Authorization (Application: app, Category: cat, Key: k) for User ("user1")',
            'isOk' => false,
            'message' => 'Not Authorized',
            'comment' => null,
        ], $status->toJsonArray());
    }
}