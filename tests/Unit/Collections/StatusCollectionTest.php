<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 5/10/20
 * Time: 10:35 PM
 */

namespace MiamiOH\LaravelStatusCheck\Test\Unit\Collections;


use MiamiOH\LaravelStatusCheck\Collections\StatusCollection;
use MiamiOH\LaravelStatusCheck\Models\Status;
use MiamiOH\LaravelStatusCheck\Test\Unit\TestCase;

/**
 * @covers \MiamiOH\LaravelStatusCheck\Collections\StatusCollection
 * @covers \MiamiOH\LaravelStatusCheck\Collections\BaseCollection
 */
class StatusCollectionTest extends TestCase
{
    public function testApplicationIsHealthyIfAllStatusAreOk()
    {
        $status = new StatusCollection([
            new Status('db1', true, 'connected'),
            new Status('db2', true, 'connected'),
            new Status('db3', true, 'connected'),
        ]);

        $this->assertTrue($status->isHealthy());
    }

    public function testApplicationIsNotHealthyIfAnyStatusIsNotOk()
    {
        $status = new StatusCollection([
            new Status('db1', true, 'connected'),
            new Status('db2', false, 'connected'),
            new Status('db3', true, 'connected'),
        ]);

        $this->assertFalse($status->isHealthy());
    }

    public function testCovertCollectionToArray()
    {
        $status = new StatusCollection([
            new Status('db1', true, 'connected'),
            new Status('db2', false, 'connected'),
            new Status('db3', true, 'connected'),
        ]);

        $this->assertSame([
            [
                'name' => 'db1',
                'isOk' => true,
                'message' => 'connected',
                'comment' => null,
            ],
            [
                'name' => 'db2',
                'isOk' => false,
                'message' => 'connected',
                'comment' => null,
            ],
            [
                'name' => 'db3',
                'isOk' => true,
                'message' => 'connected',
                'comment' => null,
            ],
        ], $status->toJsonArray());
    }
}