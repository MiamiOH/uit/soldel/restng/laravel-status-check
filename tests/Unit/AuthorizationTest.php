<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 5/11/20
 * Time: 3:58 PM
 */

namespace MiamiOH\LaravelStatusCheck\Test\Unit;

use Illuminate\Contracts\Config\Repository;
use MiamiOH\LaravelRestng\Responses\RestngResponse;
use MiamiOH\LaravelRestng\RestngClient;
use MiamiOH\LaravelStatusCheck\Authorization;
use PHPUnit\Framework\MockObject\MockObject;
use Subfission\Cas\CasManager;

/**
 * @covers \MiamiOH\LaravelStatusCheck\Authorization
 */
class AuthorizationTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $cas;
    /**
     * @var MockObject
     */
    private $restngClient;
    /**
     * @var MockObject
     */
    private $configRepository;
    /**
     * @var Authorization
     */
    private $authorization;

    protected function setUp(): void
    {
        parent::setUp();

        $this->cas = $this->createMock(CasManager::class);
        $this->restngClient = $this->createMock(RestngClient::class);
        $this->configRepository = $this->createMock(Repository::class);
        $this->authorization = new Authorization(
            $this->cas,
            $this->restngClient,
            $this->configRepository
        );
    }

    public function testAnyPersonCanAccessTheStatusPage()
    {
        $this->mockGetFromConfig([
            'anonymous',
        ]);

        $this->assertTrue($this->authorization->isAuthorized());
    }

    public function testAnonymousUserCannotAccess()
    {
        $this->mockGetFromConfig([]);
        $this->cas->method('isAuthenticated')->willReturn(false);

        $this->assertFalse($this->authorization->isAuthorized());
    }

    public function testAnyPersonLoginWithCasCanAccess()
    {
        $this->mockGetFromConfig([
            'cas',
        ]);

        $this->cas->method('isAuthenticated')->willReturn(true);

        $this->assertTrue($this->authorization->isAuthorized());
    }

    public function testUserWithInvalidTokenCannotAccess()
    {
        $this->mockGetFromConfig([
            [
                'application' => 'app',
                'category' => 'cat',
                'key' => 'k',
            ],
        ]);
        $this->cas->method('isAuthenticated')->willReturn(false);
        $this->restngClient->method('get')
            ->with($this->equalTo('/authentication/v1/token123'))
            ->willThrowException(new \Exception('invalid token'));
        $this->assertFalse($this->authorization->isAuthorized('token123'));
    }

    public function testUserIsAuthorizedIfAnyAuthConfigMatches()
    {
        $this->mockGetFromConfig([
            [
                'application' => 'app1',
                'category' => 'cat1',
                'key' => 'k1',
            ],
            [
                'application' => 'app2',
                'category' => 'cat2',
                'key' => 'k2',
            ],
        ]);
        $this->cas->method('isAuthenticated')->willReturn(true);
        $this->cas->method('user')->willReturn('user1');

        $this->restngClient
            ->expects($this->at(0))
            ->method('get')
            ->with(
                $this->equalTo('/authorization/v1/check/app1/cat1/k1'),
                $this->equalTo([
                    'username' => 'user1',
                ])
            )
            ->willReturn(new RestngResponse([
                'authorized' => false,
            ], 200));

        $this->restngClient
            ->expects($this->at(1))
            ->method('get')
            ->with(
                $this->equalTo('/authorization/v1/check/app2/cat2/k2'),
                $this->equalTo([
                    'username' => 'user1',
                ])
            )
            ->willReturn(new RestngResponse([
                'authorized' => true,
            ], 200));

        $this->assertTrue($this->authorization->isAuthorized(null));
    }

    public function testUserIsNotAuthorizedIfAllAuthConfigsDoNotMatch()
    {
        $this->mockGetFromConfig([
            [
                'application' => 'app1',
                'category' => 'cat1',
                'key' => 'k1',
            ],
            [
                'application' => 'app2',
                'category' => 'cat2',
                'key' => 'k2',
            ],
        ]);
        $this->cas->method('isAuthenticated')->willReturn(false);
        $this->restngClient
            ->expects($this->at(0))
            ->method('get')
            ->with($this->equalTo('/authentication/v1/token123'))
            ->willReturn(new RestngResponse([
                'username' => 'user1',
            ], 200));

        $this->restngClient
            ->expects($this->at(1))
            ->method('get')
            ->with(
                $this->equalTo('/authorization/v1/check/app1/cat1/k1'),
                $this->equalTo([
                    'username' => 'user1',
                ])
            )
            ->willThrowException(new \Exception('err'));

        $this->restngClient
            ->expects($this->at(2))
            ->method('get')
            ->with(
                $this->equalTo('/authorization/v1/check/app2/cat2/k2'),
                $this->equalTo([
                    'username' => 'user1',
                ])
            )
            ->willReturn(new RestngResponse([], 401));

        $this->assertFalse($this->authorization->isAuthorized('token123'));
    }

    private function mockGetFromConfig(array $arr)
    {
        $this->configRepository->method('get')
            ->with(
                $this->equalTo('status.authorization'),
                $this->equalTo([])
            )
            ->willReturn($arr);
    }
}