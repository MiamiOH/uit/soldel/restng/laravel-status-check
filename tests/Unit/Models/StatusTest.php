<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 5/10/20
 * Time: 10:31 PM
 */

namespace MiamiOH\LaravelStatusCheck\Test\Unit\Models;


use MiamiOH\LaravelStatusCheck\Models\Status;
use MiamiOH\LaravelStatusCheck\Test\Unit\TestCase;

/** @covers \MiamiOH\LaravelStatusCheck\Models\Status */
class StatusTest extends TestCase
{
    public function testConvertStatusToArray()
    {
        $status = new Status(
            'DB Connection',
            true,
            'connected',
            'everything ok'
        );

        $this->assertSame([
            'name' => 'DB Connection',
            'isOk' => true,
            'message' => 'connected',
            'comment' => 'everything ok',
        ], $status->toJsonArray());
    }

    public function testCreateStatusFromArray()
    {
        $arr = [
            'name' => 'DB Connection',
            'isOk' => true,
            'message' => 'connected',
            'comment' => 'everything ok',
        ];

        $status = Status::createFromArray($arr);
        $this->assertInstanceOf(Status::class, $status);
        $this->assertSame($arr, $status->toJsonArray());
    }
}