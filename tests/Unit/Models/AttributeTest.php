<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 5/10/20
 * Time: 8:42 PM
 */

namespace MiamiOH\LaravelStatusCheck\Test\Unit\Models;


use MiamiOH\LaravelStatusCheck\Models\Attribute;
use MiamiOH\LaravelStatusCheck\Test\Unit\TestCase;

/**
 * @covers \MiamiOH\LaravelStatusCheck\Models\Attribute
 */
class AttributeTest extends TestCase
{
    public function testConvertAttributeToArray() {
        $attribute = new Attribute(
            'environment',
            'Environment',
            'local'
        );

        $this->assertSame([
            'key' => 'environment',
            'description' => 'Environment',
            'value' => 'local'
        ], $attribute->toJsonArray());
    }
}