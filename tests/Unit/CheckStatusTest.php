<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 5/11/20
 * Time: 6:00 PM
 */

namespace MiamiOH\LaravelStatusCheck\Test\Unit;

use Illuminate\Contracts\Cache\Repository as CacheRepository;
use Illuminate\Contracts\Config\Repository as ConfigRepository;
use Illuminate\Contracts\Container\Container;
use MiamiOH\LaravelStatusCheck\Checkers\Database\DatabaseConnectionChecker;
use MiamiOH\LaravelStatusCheck\Checkers\Database\DatabaseMigrationChecker;
use MiamiOH\LaravelStatusCheck\Checkers\Restng\RestngAuthorizationChecker;
use MiamiOH\LaravelStatusCheck\Checkers\Restng\RestngCredentialChecker;
use MiamiOH\LaravelStatusCheck\Checkers\Restng\RestngReachableChecker;
use MiamiOH\LaravelStatusCheck\Checkers\Restng\RestngResourceChecker;
use MiamiOH\LaravelStatusCheck\Checkers\SftpConnectionChecker;
use MiamiOH\LaravelStatusCheck\Checkers\StatusChecker;
use MiamiOH\LaravelStatusCheck\CheckStatus;
use MiamiOH\LaravelStatusCheck\CheckStatusRequest;
use MiamiOH\LaravelStatusCheck\Collections\CheckStatusRequestCollection;
use MiamiOH\LaravelStatusCheck\Models\Status;
use MiamiOH\LaravelStatusCheck\Resolvers\ParamResolver;
use MiamiOH\LaravelStatusCheck\Resolvers\ValueResolver;
use PHPUnit\Framework\MockObject\MockObject;

class CheckStatusTestParamResolver implements ParamResolver
{
    public function getParams(): array
    {
        return [];
    }
}

/**
 * @covers \MiamiOH\LaravelStatusCheck\CheckStatus
 */
class CheckStatusTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $configRepository;
    /**
     * @var MockObject
     */
    private $container;
    /**
     * @var MockObject
     */
    private $cache;
    /**
     * @var CheckStatus
     */
    private $checkStatus;

    protected function setUp(): void
    {
        parent::setUp();

        $this->configRepository = $this->createMock(ConfigRepository::class);
        $this->container = $this->createMock(Container::class);
        $this->cache = $this->createMock(CacheRepository::class);
        $this->checkStatus = new CheckStatus(
            $this->configRepository,
            $this->container,
            $this->cache
        );
    }

    public function testGetPlainValueAttributes()
    {
        $this->configRepository->method('get')
            ->with(
                $this->equalTo('status.attributes'),
                $this->equalTo([])
            )
            ->willReturn([
                'environment' => [
                    'description' => 'Environment',
                    'value' => 'local',
                ],
                'abc' => [
                    'description' => 'Abc',
                    'value' => '123',
                ],
            ]);

        $attributes = $this->checkStatus->getAttributes();

        $this->assertCount(2, $attributes);
        $this->assertSame([
            [
                'key' => 'environment',
                'description' => 'Environment',
                'value' => 'local',
            ],
            [
                'key' => 'abc',
                'description' => 'Abc',
                'value' => '123',
            ],
        ], $attributes->toJsonArray());
    }

    public function testGetAttributesWithResolvers()
    {
        $this->configRepository->method('get')
            ->with(
                $this->equalTo('status.attributes'),
                $this->equalTo([])
            )
            ->willReturn([
                'environment' => [
                    'description' => 'Environment',
                    'resolver' => 'resolver1',
                ],
            ]);

        $resolver = $this->createMock(ValueResolver::class);
        $resolver->method('getValue')->willReturn('local');
        $this->container->method('get')->willReturn($resolver);

        $attributes = $this->checkStatus->getAttributes();

        $this->assertSame([
            [
                'key' => 'environment',
                'description' => 'Environment',
                'value' => 'local',
            ],
        ], $attributes->toJsonArray());
    }

    public function testGetDatabaseCheckStatusRequests()
    {
        $this->configRepository->method('get')
            ->with(
                $this->equalTo('status.db'),
                $this->equalTo([])
            )
            ->willReturn([
                'oracle' => [
                    'checkMigrations' => true,
                ],
                'mysql' => [
                    'checkMigrations' => false,
                ],
            ]);

        $requests = $this->checkStatus->getDatabaseCheckStatusRequests();

        $this->assertCount(3, $requests);
        $this->assertSame([
            [
                'checker' => DatabaseConnectionChecker::class,
                'params' => [
                    'name' => 'oracle',
                ],
            ],
            [
                'checker' => DatabaseMigrationChecker::class,
                'params' => [
                    'name' => 'oracle',
                ],
            ],
            [
                'checker' => DatabaseConnectionChecker::class,
                'params' => [
                    'name' => 'mysql',
                ],
            ],
        ], $requests->toJsonArray());
    }

    public function testGetRestngCheckStatusRequests()
    {
        $this->configRepository
            ->expects($this->at(0))
            ->method('get')
            ->with(
                $this->equalTo('status.restng.checkReachable'),
                $this->equalTo(false)
            )
            ->willReturn(true);

        $this->configRepository
            ->expects($this->at(1))
            ->method('get')
            ->with(
                $this->equalTo('status.restng.verifyCredentials'),
                $this->equalTo([])
            )
            ->willReturn([
                'cre1',
                'cre2',
            ]);

        $this->configRepository
            ->expects($this->at(2))
            ->method('get')
            ->with(
                $this->equalTo('status.restng.verifyAuthorizations'),
                $this->equalTo([])
            )
            ->willReturn([
                [
                    'application' => 'app1',
                    'category' => 'cat1',
                    'key' => 'key1',
                    'credentialName' => 'cre1',
                ],
                [
                    'application' => 'app2',
                    'category' => 'cat2',
                    'key' => 'key2',
                    'credentialName' => 'cre2',
                ],
            ]);

        $this->configRepository
            ->expects($this->at(3))
            ->method('get')
            ->with(
                $this->equalTo('status.restng.checkResources'),
                $this->equalTo([])
            )
            ->willReturn([
                'random.string',
                'auth.v1',
            ]);

        $requests = $this->checkStatus->getRestngCheckStatusRequests();

        $this->assertCount(7, $requests);
        $this->assertSame([
            [
                'checker' => RestngReachableChecker::class,
                'params' => [],
            ],
            [
                'checker' => RestngCredentialChecker::class,
                'params' => [
                    'name' => 'cre1',
                ],
            ],
            [
                'checker' => RestngCredentialChecker::class,
                'params' => [
                    'name' => 'cre2',
                ],
            ],
            [
                'checker' => RestngAuthorizationChecker::class,
                'params' => [
                    'application' => 'app1',
                    'category' => 'cat1',
                    'key' => 'key1',
                    'credentialName' => 'cre1',
                ],
            ],
            [
                'checker' => RestngAuthorizationChecker::class,
                'params' => [
                    'application' => 'app2',
                    'category' => 'cat2',
                    'key' => 'key2',
                    'credentialName' => 'cre2',
                ],
            ],
            [
                'checker' => RestngResourceChecker::class,
                'params' => [
                    'name' => 'random.string',
                ],
            ],
            [
                'checker' => RestngResourceChecker::class,
                'params' => [
                    'name' => 'auth.v1',
                ],
            ],
        ], $requests->toJsonArray());
    }

    public function testGetCustomCheckStatusRequests()
    {
        $this->configRepository->method('get')
            ->with(
                $this->equalTo('status.custom'),
                $this->equalTo([])
            )
            ->willReturn([
                DatabaseConnectionChecker::class => [
                    [
                        'name' => 'oracle',
                    ],
                    [
                        'name' => 'mysql',
                    ],
                ],
                SftpConnectionChecker::class => CheckStatusTestParamResolver::class,
            ]);

        $resolver = $this->createMock(CheckStatusTestParamResolver::class);
        $resolver->method('getParams')->willReturn([
            [
                'name' => 'disk1',
            ],
            [
                'name' => 'disk2',
            ],
        ]);
        $this->container->method('get')
            ->with($this->equalTo(CheckStatusTestParamResolver::class))
            ->willReturn($resolver);

        $requests = $this->checkStatus->getCustomCheckStatusRequests();

        $this->assertCount(4, $requests);
        $this->assertSame([
            [
                'checker' => DatabaseConnectionChecker::class,
                'params' => [
                    'name' => 'oracle',
                ],
            ],
            [
                'checker' => DatabaseConnectionChecker::class,
                'params' => [
                    'name' => 'mysql',
                ],
            ],
            [
                'checker' => SftpConnectionChecker::class,
                'params' => [
                    'name' => 'disk1',
                ],
            ],
            [
                'checker' => SftpConnectionChecker::class,
                'params' => [
                    'name' => 'disk2',
                ],
            ],
        ], $requests->toJsonArray());
    }

    public function testGetAllRequests()
    {
        $this->configRepository->method('get')
            ->willReturnMap([
                [
                    'status.db',
                    [],
                    [
                        'oracle' => [
                            'checkMigrations' => true,
                        ],
                    ],
                ],
                ['status.restng.checkReachable', false, true],
                [
                    'status.restng.verifyCredentials',
                    [],
                    [
                        'default',
                    ],
                ],
                [
                    'status.restng.verifyAuthorizations',
                    [],
                    [
                        [
                            'application' => 'WebServices',
                            'category' => 'AccountReceivable',
                            'key' => 'postTransaction',
                            'credentialName' => 'default',

                        ],
                    ],
                ],
                [
                    'status.restng.checkResources',
                    [],
                    [
                        'random.string',
                    ],
                ],
                [
                    'status.custom',
                    [],
                    [
                        DatabaseConnectionChecker::class => [
                            [
                                'name' => 'mysql',
                            ],
                        ],
                    ],
                ],
            ]);

        $requests = $this->checkStatus->getAllCheckStatusRequests();
        $this->assertCount(7, $requests);
    }

    public function testCheckStatusFromCache()
    {
        $this->configRepository->method('get')
            ->with($this->equalTo('status.cacheTimeout'))
            ->willReturn(10);

        $request = new CheckStatusRequest(
            DatabaseConnectionChecker::class,
            ['name' => 'mysql']
        );

        $key = md5(json_encode($request->toJsonArray()));
        $this->cache->method('has')
            ->with($this->equalTo($key))
            ->willReturn(true);
        $statusArr = [
            'name' => 'Db Connection',
            'isOk' => true,
            'message' => 'connected',
            'comment' => null,
        ];
        $this->cache->method('get')
            ->with($this->equalTo($key))
            ->willReturn(json_encode($statusArr));
        $status = $this->checkStatus->check($request, false);
        $this->assertSame($statusArr, $status->toJsonArray());
    }

    public function testCheckStatusWithoutCache()
    {
        $this->configRepository->method('get')
            ->with($this->equalTo('status.cacheTimeout'))
            ->willReturn(0);

        $request = new CheckStatusRequest(
            DatabaseConnectionChecker::class,
            ['name' => 'mysql']
        );

        $checker = $this->createMock(StatusChecker::class);
        $this->container->method('get')
            ->with($this->equalTo(DatabaseConnectionChecker::class))
            ->willReturn($checker);
        $status = new Status('Db Connection', true, 'Connected', null);
        $checker->method('check')
            ->with($this->equalTo(['name' => 'mysql']))
            ->willReturn($status);

        $this->assertSame($status, $this->checkStatus->check($request));
    }

    public function testCheckStatusWithException()
    {
        $this->configRepository->method('get')
            ->with($this->equalTo('status.cacheTimeout'))
            ->willReturn(0);

        $request = new CheckStatusRequest(
            DatabaseConnectionChecker::class,
            ['name' => 'mysql']
        );

        $checker = $this->createMock(StatusChecker::class);
        $this->container->method('get')
            ->with($this->equalTo(DatabaseConnectionChecker::class))
            ->willReturn($checker);
        $checker->method('check')->willThrowException(new \Exception('abcd'));
        $checker->method('getName')->willReturn('Db Conn');

        $this->assertSame([
            'name' => 'Db Conn',
            'isOk' => false,
            'message' => 'Error',
            'comment' => 'abcd',
        ], $this->checkStatus->check($request)->toJsonArray());
    }

    public function testForceRefreshStatusCheckRegardlessCache()
    {
        $this->configRepository->method('get')
            ->with($this->equalTo('status.cacheTimeout'))
            ->willReturn(10);

        $request = new CheckStatusRequest(
            DatabaseConnectionChecker::class,
            ['name' => 'mysql']
        );

        $checker = $this->createMock(StatusChecker::class);
        $this->container->method('get')
            ->with($this->equalTo(DatabaseConnectionChecker::class))
            ->willReturn($checker);
        $status = new Status('Db Connection', true, 'Connected', null);
        $checker->method('check')
            ->with($this->equalTo(['name' => 'mysql']))
            ->willReturn($status);

        $this->cache->method('put')
            ->with(
                $this->equalTo(md5(json_encode($request->toJsonArray()))),
                $this->equalTo(json_encode($status->toJsonArray())),
                $this->equalTo(10)
            )
            ->willReturn(true);

        $this->assertSame($status, $this->checkStatus->check($request, true));
    }

    public function testCheckStatusIfNotFoundInTheCache()
    {
        $this->configRepository->method('get')
            ->with($this->equalTo('status.cacheTimeout'))
            ->willReturn(10);

        $request = new CheckStatusRequest(
            DatabaseConnectionChecker::class,
            ['name' => 'mysql']
        );

        $checker = $this->createMock(StatusChecker::class);
        $this->container->method('get')
            ->with($this->equalTo(DatabaseConnectionChecker::class))
            ->willReturn($checker);
        $status = new Status('Db Connection', true, 'Connected', null);
        $checker->method('check')
            ->with($this->equalTo(['name' => 'mysql']))
            ->willReturn($status);
        $this->cache->method('has')
            ->with($this->equalTo(md5(json_encode($request->toJsonArray()))))
            ->willReturn(false);
        $this->cache->method('put')
            ->with(
                $this->equalTo(md5(json_encode($request->toJsonArray()))),
                $this->equalTo(json_encode($status->toJsonArray())),
                $this->equalTo(10)
            )
            ->willReturn(true);

        $this->assertSame($status, $this->checkStatus->check($request, false));
    }

    public function testCheckAllStatus()
    {
        $this->configRepository->method('get')
            ->with($this->equalTo('status.cacheTimeout'))
            ->willReturn(10);

        $requests = new CheckStatusRequestCollection([
            new CheckStatusRequest(DatabaseConnectionChecker::class, ['name' => 'oracle']),
            new CheckStatusRequest(DatabaseConnectionChecker::class, ['name' => 'mysql']),
        ]);

        $this->cache->method('has')->willReturn(true);
        $this->cache->method('get')->willReturn(json_encode((new Status('a', true, 'l'))->toJsonArray()));
        $this->assertCount(2, $this->checkStatus->checkAll($requests, false));
    }
}