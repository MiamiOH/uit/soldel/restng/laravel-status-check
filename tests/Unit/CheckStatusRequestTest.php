<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 5/11/20
 * Time: 12:14 AM
 */

namespace MiamiOH\LaravelStatusCheck\Test\Unit;

use Illuminate\Contracts\Container\Container;
use MiamiOH\LaravelStatusCheck\Checkers\StatusChecker;
use MiamiOH\LaravelStatusCheck\CheckStatusRequest;

/**
 * @covers \MiamiOH\LaravelStatusCheck\CheckStatusRequest
 */
class CheckStatusRequestTest extends TestCase
{
    public function testConvertRequestToArray()
    {
        $request = new CheckStatusRequest('checker1', [
            'name' => 'aaa',
        ]);

        $this->assertSame([
            'checker' => 'checker1',
            'params' => [
                'name' => 'aaa',
            ],
        ], $request->toJsonArray());
    }

    public function testCreateRequestFromArray()
    {
        $arr = [
            'checker' => 'checker1',
            'params' => [
                'name' => 'aaa',
            ],
        ];

        $request = CheckStatusRequest::createFromArray($arr);

        $this->assertSame($arr, $request->toJsonArray());
    }

    public function testGetNameFromChecker()
    {
        $container = $this->createMock(Container::class);

        $checker = $this->createMock(StatusChecker::class);
        $checker->method('getName')
            ->with($this->equalTo(['name' => 'aaa']))
            ->willReturn('askdflasdjf');

        $container->method('get')
            ->with($this->equalTo('checker1'))
            ->willReturn($checker);

        $request = new CheckStatusRequest('checker1', [
            'name' => 'aaa',
        ]);

        $this->assertSame('askdflasdjf', $request->getCheckerName($container));
    }
}