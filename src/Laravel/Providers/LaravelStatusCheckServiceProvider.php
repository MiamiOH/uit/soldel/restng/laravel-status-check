<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 10/3/18
 * Time: 9:15 PM
 */

namespace MiamiOH\LaravelStatusCheck\Laravel\Providers;

use Illuminate\Contracts\Cache\Repository as CacheRepository;
use Illuminate\Contracts\Config\Repository as ConfigRepository;
use Illuminate\Contracts\Console\Kernel;
use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Filesystem\Factory;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\ServiceProvider;
use MiamiOH\LaravelRestng\Laravel\CredentialPool;
use MiamiOH\LaravelRestng\RestngClient;
use MiamiOH\LaravelStatusCheck\Authorization;
use MiamiOH\LaravelStatusCheck\Checkers\Database\DatabaseConnectionChecker;
use MiamiOH\LaravelStatusCheck\Checkers\Database\DatabaseMigrationChecker;
use MiamiOH\LaravelStatusCheck\Checkers\Restng\RestngAuthorizationChecker;
use MiamiOH\LaravelStatusCheck\Checkers\Restng\RestngCredentialChecker;
use MiamiOH\LaravelStatusCheck\Checkers\Restng\RestngReachableChecker;
use MiamiOH\LaravelStatusCheck\Checkers\Restng\RestngResourceChecker;
use MiamiOH\LaravelStatusCheck\Checkers\SftpConnectionChecker;
use MiamiOH\LaravelStatusCheck\CheckStatus;

class LaravelStatusCheckServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../../../config/status.php' => config_path('status.php'),
        ], 'status-config');

        $this->loadRoutesFrom(__DIR__.'/../../../routes.php');

        $this->loadViewsFrom(__DIR__.'/../../../views', 'status');

        $this->publishes([
            __DIR__.'/../../../views' => resource_path('views/vendor/status'),
        ], 'status-views');

        $this->mergeConfigFrom(
            __DIR__.'/../../../config/status.php',
            'status'
        );
    }

    public function register()
    {
        $this->app->singleton(DatabaseConnectionChecker::class, function () {
            return new DatabaseConnectionChecker(
                resolve(DatabaseManager::class),
                resolve(Kernel::class)
            );
        });

        $this->app->singleton(DatabaseMigrationChecker::class, function () {
            return new DatabaseMigrationChecker(
                resolve(DatabaseManager::class),
                resolve(Kernel::class)
            );
        });

        $this->app->singleton(RestngReachableChecker::class, function () {
            return new RestngReachableChecker(
                resolve(RestngClient::class),
                resolve(CredentialPool::class)
            );
        });

        $this->app->singleton(RestngResourceChecker::class, function () {
            return new RestngResourceChecker(
                resolve(RestngClient::class),
                resolve(CredentialPool::class)
            );
        });

        $this->app->singleton(RestngCredentialChecker::class, function () {
            return new RestngCredentialChecker(
                resolve(RestngClient::class),
                resolve(CredentialPool::class)
            );
        });

        $this->app->singleton(RestngAuthorizationChecker::class, function () {
            return new RestngAuthorizationChecker(
                resolve(RestngClient::class),
                resolve(CredentialPool::class)
            );
        });

        $this->app->singleton(SftpConnectionChecker::class, function () {
            return new SftpConnectionChecker(
                resolve(Factory::class)
            );
        });

        $this->app->singleton(Authorization::class, function () {
            return new Authorization(
                resolve('cas'),
                resolve(RestngClient::class),
                resolve(ConfigRepository::class)
            );
        });

        $this->app->singleton(CheckStatus::class, function () {
            return new CheckStatus(
                resolve(ConfigRepository::class),
                resolve(Container::class),
                resolve(CacheRepository::class)
            );
        });
    }

    public function provides()
    {
        return [
            CheckStatus::class,
        ];
    }
}
