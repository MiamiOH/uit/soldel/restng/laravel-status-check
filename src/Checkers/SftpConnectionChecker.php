<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 5/9/20
 * Time: 5:26 PM
 */

namespace MiamiOH\LaravelStatusCheck\Checkers;

use Illuminate\Contracts\Filesystem\Factory;
use MiamiOH\LaravelStatusCheck\Models\Status;

class SftpConnectionChecker implements StatusChecker
{
    /**
     * @var Factory
     */
    private $fsFactory;

    /**
     * SftpConnectionChecker constructor.
     * @param Factory $fsFactory
     */
    public function __construct(Factory $fsFactory)
    {
        $this->fsFactory = $fsFactory;
    }

    public function check(array $params = []): Status
    {
        $name = $this->getName($params);

        $fs = $this->fsFactory->disk($params['name']);

        try {
            $fs->getDriver()->getAdapter()->connect();

            return new Status($name, true, 'Connected');
        } catch (\Exception $e) {
            return new Status($name, false, 'Not Connected', $e->getMessage());
        }
    }

    public function getName(array $params = []): string
    {
        return sprintf('SFTP Connection [%s]', $params['name']);
    }
}
