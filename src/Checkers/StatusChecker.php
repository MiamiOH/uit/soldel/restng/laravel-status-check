<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 5/7/20
 * Time: 6:37 PM
 */

namespace MiamiOH\LaravelStatusCheck\Checkers;

use MiamiOH\LaravelStatusCheck\Models\Status;

interface StatusChecker
{
    /**
     * The function is called at runtime to check whether
     * the resource is healthy or not.
     *
     * Example:
     *   MiamiOH\LaravelStatusCheck\Checkers\Database\DatabaseConnectionChecker
     *
     * @param array $params
     * @return Status
     */
    public function check(array $params = []): Status;

    /**
     * Get resource name.
     *
     * @param array $params
     * @return string
     */
    public function getName(array $params = []): string;
}
