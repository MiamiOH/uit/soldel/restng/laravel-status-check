<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 5/7/20
 * Time: 6:38 PM
 */

namespace MiamiOH\LaravelStatusCheck\Checkers\Database;

use MiamiOH\LaravelStatusCheck\Checkers\StatusChecker;
use MiamiOH\LaravelStatusCheck\Models\Status;
use Symfony\Component\Console\Output\BufferedOutput;

class DatabaseMigrationChecker extends BaseDatabaseChecker implements StatusChecker
{
    public function check(array $params = []): Status
    {
        $name = $this->getName($params);

        $buffer = new BufferedOutput();
        $this->getConsole()->call('migrate:status', [
            '--database' => $params['name'],
        ], $buffer);
        $output = $buffer->fetch();

        if (strpos($output, 'No migrations found') !== false) {
            return new Status($name, false, 'No migrations found');
        }

        if (strpos($output, 'No') !== false || strpos($output, 'Migration table not found') !== false) {
            return new Status($name, false, 'Not Migrated', $output);
        }

        return new Status($name, true, 'Migrated');
    }

    public function getName(array $params = []): string
    {
        return sprintf('Database Migration [%s]', $params['name']);
    }
}
