<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 5/7/20
 * Time: 6:39 PM
 */

namespace MiamiOH\LaravelStatusCheck\Checkers\Database;

use Illuminate\Contracts\Console\Kernel;
use Illuminate\Database\DatabaseManager;

abstract class BaseDatabaseChecker
{
    /**
     * @var DatabaseManager
     */
    private $dbFactory;
    /**
     * @var Kernel
     */
    private $console;

    /**
     * BaseDatabaseChecker constructor.
     * @param DatabaseManager $dbFactory
     * @param Kernel $console
     */
    public function __construct(DatabaseManager $dbFactory, Kernel $console)
    {
        $this->dbFactory = $dbFactory;
        $this->console = $console;
    }

    /**
     * @return DatabaseManager
     */
    public function getDbFactory(): DatabaseManager
    {
        return $this->dbFactory;
    }

    /**
     * @return Kernel
     */
    public function getConsole(): Kernel
    {
        return $this->console;
    }
}
