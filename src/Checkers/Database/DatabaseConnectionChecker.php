<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 5/7/20
 * Time: 6:38 PM
 */

namespace MiamiOH\LaravelStatusCheck\Checkers\Database;

use MiamiOH\LaravelStatusCheck\Checkers\StatusChecker;
use MiamiOH\LaravelStatusCheck\Models\Status;

class DatabaseConnectionChecker extends BaseDatabaseChecker implements StatusChecker
{
    public function check(array $params = []): Status
    {
        $name = $this->getName($params);
        $this->getDbFactory()->connection($params['name'])->getPdo();

        return new Status($name, true, 'Connected');
    }

    public function getName(array $params = []): string
    {
        return sprintf('Database Connection [%s]', $params['name']);
    }
}
