<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 5/7/20
 * Time: 6:50 PM
 */

namespace MiamiOH\LaravelStatusCheck\Checkers\Restng;

use MiamiOH\LaravelStatusCheck\Checkers\StatusChecker;
use MiamiOH\LaravelStatusCheck\Models\Status;

class RestngAuthorizationChecker extends BaseRestngChecker implements StatusChecker
{
    public function check(array $params = []): Status
    {
        $application = $params['application'];
        $category = $params['category'];
        $key = $params['key'];
        $credentialName = $params['credentialName'];

        $credential = $this->getCredentialPool()->get($credentialName);
        $user = $credential->getUsername();

        $name = $this->getName($params);

        $res = $this->getRestngClient()->get(sprintf(
            '/authorization/v1/%s/%s/%s',
            $application,
            $category,
            $key
        ), ['username' => $user]);

        $isOk = $res->getData()['allowed'] === 'true';

        return new Status(
            $name,
            $isOk,
            $isOk ? 'Authorized' : 'Not Authorized'
        );
    }

    public function getName(array $params = []): string
    {
        $application = $params['application'];
        $category = $params['category'];
        $key = $params['key'];
        $credentialName = $params['credentialName'];

        $credential = $this->getCredentialPool()->get($credentialName);
        $user = $credential->getUsername();

        return sprintf(
            'RESTng Authorization (Application: %s, Category: %s, Key: %s) for User ("%s")',
            $application,
            $category,
            $key,
            $user
        );
    }
}
