<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 5/7/20
 * Time: 6:50 PM
 */

namespace MiamiOH\LaravelStatusCheck\Checkers\Restng;

use MiamiOH\LaravelStatusCheck\Checkers\StatusChecker;
use MiamiOH\LaravelStatusCheck\Models\Status;

class RestngCredentialChecker extends BaseRestngChecker implements StatusChecker
{
    public function check(array $params = []): Status
    {
        $credential = $this->getCredentialPool()->get($params['name']);

        $name = $this->getName($params);
        $isOk = $credential->isValid($this->getRestngClient());

        return new Status(
            $name,
            $isOk,
            $isOk ? 'Authenticated' : 'Not Authenticated: invalid username/password'
        );
    }

    public function getName(array $params = []): string
    {
        $credential = $this->getCredentialPool()->get($params['name']);

        return sprintf('RESTng Authentication: %s', $credential->getUsername());
    }
}
