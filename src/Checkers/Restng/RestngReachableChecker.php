<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 5/7/20
 * Time: 6:50 PM
 */

namespace MiamiOH\LaravelStatusCheck\Checkers\Restng;

use MiamiOH\LaravelStatusCheck\Checkers\StatusChecker;
use MiamiOH\LaravelStatusCheck\Models\Status;

class RestngReachableChecker extends BaseRestngChecker implements StatusChecker
{
    public function check(array $params = []): Status
    {
        $name = $this->getName($params);
        $res = $this->getRestngClient()->get('/api');
        $isOk = $res->getStatusCode() === 200;

        return new Status(
            $name,
            $isOk,
            $isOk ? 'Operational' : 'Not Reachable'
        );
    }

    public function getName(array $params = []): string
    {
        return sprintf('RESTng: %s', $this->getRestngClient()->getRestngUrl());
    }
}
