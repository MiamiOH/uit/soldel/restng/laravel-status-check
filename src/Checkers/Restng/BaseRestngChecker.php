<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 5/7/20
 * Time: 6:46 PM
 */

namespace MiamiOH\LaravelStatusCheck\Checkers\Restng;

use MiamiOH\LaravelRestng\Laravel\CredentialPool;
use MiamiOH\LaravelRestng\RestngClient;

abstract class BaseRestngChecker
{
    /**
     * @var RestngClient
     */
    private $restngClient;
    /**
     * @var CredentialPool
     */
    private $credentialPool;

    /**
     * BaseRestngChecker constructor.
     * @param RestngClient $restngClient
     * @param CredentialPool $credentialPool
     */
    public function __construct(RestngClient $restngClient, CredentialPool $credentialPool)
    {
        $this->restngClient = $restngClient;
        $this->credentialPool = $credentialPool;
    }

    /**
     * @return RestngClient
     */
    public function getRestngClient(): RestngClient
    {
        return $this->restngClient;
    }

    /**
     * @return CredentialPool
     */
    public function getCredentialPool(): CredentialPool
    {
        return $this->credentialPool;
    }
}
