<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 5/7/20
 * Time: 6:50 PM
 */

namespace MiamiOH\LaravelStatusCheck\Checkers\Restng;

use MiamiOH\LaravelStatusCheck\Checkers\StatusChecker;
use MiamiOH\LaravelStatusCheck\Models\Status;

class RestngResourceChecker extends BaseRestngChecker implements StatusChecker
{
    public function check(array $params = []): Status
    {
        $name = $this->getName($params);
        $res = $this->getRestngClient()->get(sprintf('/api/resource/%s', $params['name']));
        $isOk = $res->getStatusCode() === 200;

        if (!$isOk) {
            return new Status(
                $name,
                false,
                'Not Found'
            );
        }

        $body = $res->getData();
        $action = $body['action'];
        $pattern = $body['pattern'];

        $method = null;
        switch ($action) {
            case 'create':
                $method = 'POST';
                break;
            case 'update':
                $method = 'PUT';
                break;
            case 'read':
                $method = 'GET';
                break;
            case 'delete':
                $method = 'DELETE';
                break;
            default:
                $method = $action;
        }

        $comment = sprintf('%s %s', $method, $pattern);

        return new Status(
            $name,
            true,
            'Configured',
            $comment
        );
    }

    public function getName(array $params = []): string
    {
        return sprintf('RESTng Resource: %s', $params['name']);
    }
}
