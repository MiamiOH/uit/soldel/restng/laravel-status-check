<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 5/9/20
 * Time: 7:43 PM
 */

namespace MiamiOH\LaravelStatusCheck;

use Illuminate\Contracts\Config\Repository;
use MiamiOH\LaravelRestng\RestngClient;
use Subfission\Cas\CasManager;

class Authorization
{
    /**
     * @var CasManager
     */
    private $cas;
    /**
     * @var RestngClient
     */
    private $restngClient;
    /**
     * @var Repository
     */
    private $configRepository;

    /**
     * Authorization constructor.
     * @param CasManager $cas
     * @param RestngClient $restngClient
     * @param Repository $configRepository
     */
    public function __construct(CasManager $cas, RestngClient $restngClient, Repository $configRepository)
    {
        $this->cas = $cas;
        $this->restngClient = $restngClient;
        $this->configRepository = $configRepository;
    }

    public function isAuthorized(string $token = null): bool
    {
        $authConfig = $this->configRepository->get('status.authorization', []);

        // check if visitor without login can access
        if (in_array('anonymous', $authConfig)) {
            return true;
        }

        // check if cas user can access
        if ($this->cas->isAuthenticated() && in_array('cas', $authConfig)) {
            return true;
        }

        $auths = $this->getAllAuths($authConfig);
        if (count($auths) === 0) {
            return false;
        }

        // get user from cas or from RESTng token
        $user = null;
        if ($this->cas->isAuthenticated()) {
            $user = $this->cas->user();
        } elseif ($token !== null) {
            $user = $this->getUser($token);
        }

        if ($user === null) {
            return false;
        }

        foreach ($auths as $auth) {
            if ($this->isAuthorizedByAuthman($user, $auth)) {
                return true;
            }
        }

        return false;
    }

    private function getAllAuths(array $authConfig): array
    {
        $auths = [];

        foreach ($authConfig as $auth) {
            if (is_array($auth)) {
                $auths[] = $auth;
            }
        }

        return $auths;
    }

    private function isAuthorizedByAuthman(string $user, array $auth): bool
    {
        if (!isset($auth['application'])
            || !isset($auth['category'])
            || !isset($auth['key'])) {
            return false;
        }

        try {
            $res = $this->restngClient->get(
                sprintf('/authorization/v1/check/%s/%s/%s', $auth['application'], $auth['category'], $auth['key']),
                [
                    'username' => $user,
                ]
            );

            if ($res->getStatusCode() === 401) {
                return false;
            }

            return $res->getData()['authorized'];
        } catch (\Exception $e) {
            return false;
        }
    }

    private function getUser(string $token): ?string
    {
        try {
            $res = $this->restngClient->get('/authentication/v1/'.$token);

            return $res->getData()['username'];
        } catch (\Exception $e) {
            return null;
        }
    }
}
