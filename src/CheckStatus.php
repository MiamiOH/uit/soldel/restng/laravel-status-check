<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 5/7/20
 * Time: 6:30 PM
 */

namespace MiamiOH\LaravelStatusCheck;

use Illuminate\Contracts\Cache\Repository as CacheRepository;
use Illuminate\Contracts\Config\Repository as ConfigRepository;
use Illuminate\Contracts\Container\Container;
use MiamiOH\LaravelStatusCheck\Checkers\Database\DatabaseConnectionChecker;
use MiamiOH\LaravelStatusCheck\Checkers\Database\DatabaseMigrationChecker;
use MiamiOH\LaravelStatusCheck\Checkers\Restng\RestngAuthorizationChecker;
use MiamiOH\LaravelStatusCheck\Checkers\Restng\RestngCredentialChecker;
use MiamiOH\LaravelStatusCheck\Checkers\Restng\RestngReachableChecker;
use MiamiOH\LaravelStatusCheck\Checkers\Restng\RestngResourceChecker;
use MiamiOH\LaravelStatusCheck\Checkers\StatusChecker;
use MiamiOH\LaravelStatusCheck\Collections\AttributeCollection;
use MiamiOH\LaravelStatusCheck\Collections\CheckStatusRequestCollection;
use MiamiOH\LaravelStatusCheck\Collections\StatusCollection;
use MiamiOH\LaravelStatusCheck\Models\Attribute;
use MiamiOH\LaravelStatusCheck\Models\Status;
use MiamiOH\LaravelStatusCheck\Resolvers\ParamResolver;
use MiamiOH\LaravelStatusCheck\Resolvers\ValueResolver;

class CheckStatus
{
    /**
     * @var ConfigRepository
     */
    private $configRepository;
    /**
     * @var Container
     */
    private $container;
    /**
     * @var CacheRepository
     */
    private $cache;

    /**
     * CheckStatus constructor.
     * @param ConfigRepository $configRepository
     * @param Container $container
     * @param CacheRepository $cache
     */
    public function __construct(ConfigRepository $configRepository, Container $container, CacheRepository $cache)
    {
        $this->configRepository = $configRepository;
        $this->container = $container;
        $this->cache = $cache;
    }

    public function getAttributes(): AttributeCollection
    {
        $attributes = new AttributeCollection();

        $attributeConfigs = $this->configRepository->get('status.attributes', []);
        foreach ($attributeConfigs as $key => $config) {
            $value = null;

            if (isset($config['resolver'])) {
                /** @var ValueResolver $valueResolver */
                $valueResolver = $this->container->get($config['resolver']);
                $value = $valueResolver->getValue();
            } else {
                $value = $config['value'];
            }
            $attributes->push(new Attribute(
                $key,
                $config['description'],
                $value
            ));
        }

        return $attributes;
    }

    public function getDatabaseCheckStatusRequests(): CheckStatusRequestCollection
    {
        $checkStatusRequests = new CheckStatusRequestCollection();

        $dbs = $this->configRepository->get('status.db', []);

        foreach ($dbs as $name => $options) {
            $checkStatusRequests->push(new CheckStatusRequest(
                DatabaseConnectionChecker::class,
                [
                    'name' => $name,
                ]
            ));

            if (isset($options['checkMigrations']) && $options['checkMigrations']) {
                $checkStatusRequests->push(new CheckStatusRequest(
                    DatabaseMigrationChecker::class,
                    [
                        'name' => $name,
                    ]
                ));
            }
        }

        return $checkStatusRequests;
    }

    public function getRestngCheckStatusRequests(): CheckStatusRequestCollection
    {
        $checkStatusRequests = new CheckStatusRequestCollection();

        if ($this->configRepository->get('status.restng.checkReachable', false)) {
            $checkStatusRequests->push(new CheckStatusRequest(RestngReachableChecker::class));

            $credentials = $this->configRepository->get('status.restng.verifyCredentials', []);
            foreach ($credentials as $credential) {
                $checkStatusRequests->push(new CheckStatusRequest(
                    RestngCredentialChecker::class,
                    [
                        'name' => $credential,
                    ]
                ));
            }

            $authorizations = $this->configRepository->get('status.restng.verifyAuthorizations', []);
            foreach ($authorizations as $authorization) {
                $checkStatusRequests->push(new CheckStatusRequest(
                    RestngAuthorizationChecker::class,
                    $authorization
                ));
            }

            $resources = $this->configRepository->get('status.restng.checkResources', []);
            foreach ($resources as $resource) {
                $checkStatusRequests->push(new CheckStatusRequest(
                    RestngResourceChecker::class,
                    [
                        'name' => $resource,
                    ]
                ));
            }
        }

        return $checkStatusRequests;
    }

    public function getCustomCheckStatusRequests(): CheckStatusRequestCollection
    {
        $checkStatusRequests = new CheckStatusRequestCollection();

        $customChecks = $this->configRepository->get('status.custom', []);
        foreach ($customChecks as $checker => $params) {
            if (!is_array($params)) {
                /** @var ParamResolver $paramResolver */
                $paramResolver = $this->container->get($params);
                $params = $paramResolver->getParams();
            }

            foreach ($params as $param) {
                $checkStatusRequests->push(new CheckStatusRequest(
                    $checker,
                    $param
                ));
            }
        }

        return $checkStatusRequests;
    }

    public function getAllCheckStatusRequests(): CheckStatusRequestCollection
    {
        $requests = new CheckStatusRequestCollection();

        $requests = $requests->merge($this->getDatabaseCheckStatusRequests());
        $requests = $requests->merge($this->getRestngCheckStatusRequests());
        $requests = $requests->merge($this->getCustomCheckStatusRequests());

        return $requests;
    }

    public function check(CheckStatusRequest $request, bool $forceRefresh = false): Status
    {
        $checkStatus = function (CheckStatusRequest $request) {
            /** @var StatusChecker $checker */
            $checker = $this->container->get($request->getChecker());

            try {
                $status = $checker->check($request->getParams());
            } catch (\Exception $e) {
                $status = new Status(
                    $checker->getName($request->getParams()),
                    false,
                    'Error',
                    $e->getMessage()
                );
            }

            return $status;
        };

        $cacheTimeout = $this->configRepository->get('status.cacheTimeout') ?? 0;

        if ($cacheTimeout <= 0) {
            return $checkStatus($request);
        }

        $key = md5(json_encode($request->toJsonArray()));

        if ($forceRefresh || !$this->cache->has($key)) {
            $status = $checkStatus($request);

            $this->cache->put($key, json_encode($status->toJsonArray()), $cacheTimeout);

            return $status;
        }

        return Status::createFromArray(json_decode($this->cache->get($key), true));
    }

    public function checkAll(CheckStatusRequestCollection $requests, bool $forceRefresh = false): StatusCollection
    {
        $statusCollection = new StatusCollection();

        foreach ($requests as $request) {
            $statusCollection->push($this->check($request, $forceRefresh));
        }

        return $statusCollection;
    }
}
