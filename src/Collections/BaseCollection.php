<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 5/10/20
 * Time: 7:34 PM
 */

namespace MiamiOH\LaravelStatusCheck\Collections;

use Illuminate\Support\Collection;
use MiamiOH\LaravelStatusCheck\Jsonable;

class BaseCollection extends Collection implements Jsonable
{
    public function toJsonArray(): array
    {
        $data = [];

        /** @var Jsonable $model */
        foreach ($this as $model) {
            $data[] = $model->toJsonArray();
        }

        return $data;
    }
}
