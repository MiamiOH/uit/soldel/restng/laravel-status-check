<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 5/7/20
 * Time: 6:04 PM
 */

namespace MiamiOH\LaravelStatusCheck\Collections;

use MiamiOH\LaravelStatusCheck\Models\Status;

class StatusCollection extends BaseCollection
{
    public function isHealthy(): bool
    {
        /** @var Status $status */
        foreach ($this as $status) {
            if (!$status->isOk()) {
                return false;
            }
        }

        return true;
    }
}
