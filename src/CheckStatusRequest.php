<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 5/7/20
 * Time: 7:22 PM
 */

namespace MiamiOH\LaravelStatusCheck;

use Illuminate\Contracts\Container\Container;
use MiamiOH\LaravelStatusCheck\Checkers\StatusChecker;

class CheckStatusRequest implements Jsonable
{
    /**
     * @var string
     */
    private $checker;
    /**
     * @var array
     */
    private $params;

    /**
     * CheckStatusRequest constructor.
     * @param string $checker
     * @param array $params
     */
    public function __construct(string $checker, array $params = [])
    {
        $this->checker = $checker;
        $this->params = $params;
    }

    public static function createFromArray(array $json): self
    {
        return new CheckStatusRequest(
            $json['checker'],
            $json['params']
        );
    }

    /**
     * @return string
     */
    public function getChecker(): string
    {
        return $this->checker;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

    public function getCheckerName(Container $container): string
    {
        /** @var StatusChecker $checker */
        $checker = $container->get($this->getChecker());

        return $checker->getName($this->getParams());
    }

    public function toJsonArray(): array
    {
        return [
            'checker' => $this->getChecker(),
            'params' => $this->getParams(),
        ];
    }
}
