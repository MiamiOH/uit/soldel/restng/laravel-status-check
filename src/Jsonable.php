<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 5/10/20
 * Time: 7:34 PM
 */

namespace MiamiOH\LaravelStatusCheck;

interface Jsonable
{
    public function toJsonArray(): array;
}
