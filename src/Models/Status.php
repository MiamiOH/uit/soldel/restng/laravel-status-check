<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 5/6/20
 * Time: 11:25 PM
 */

namespace MiamiOH\LaravelStatusCheck\Models;

use MiamiOH\LaravelStatusCheck\Jsonable;

final class Status implements Jsonable
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var boolean
     */
    private $isOk;
    /**
     * @var string
     */
    private $message;
    /**
     * @var string|null
     */
    private $comment;

    /**
     * Status constructor.
     * @param string $name
     * @param bool $isOk
     * @param string $message
     * @param string|null $comment
     */
    public function __construct(string $name, bool $isOk, string $message, string $comment = null)
    {
        $this->name = $name;
        $this->isOk = $isOk;
        $this->message = $message;
        $this->comment = $comment;
    }

    public static function createFromArray($data): self
    {
        return new static(
            $data['name'],
            $data['isOk'],
            $data['message'],
            $data['comment']
        );
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function isOk(): bool
    {
        return $this->isOk;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function toJsonArray(): array
    {
        return [
            'name' => $this->getName(),
            'isOk' => $this->isOk(),
            'message' => $this->getMessage(),
            'comment' => $this->getComment(),
        ];
    }
}
