<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 5/9/20
 * Time: 10:33 PM
 */

namespace MiamiOH\LaravelStatusCheck\Models;

use MiamiOH\LaravelStatusCheck\Jsonable;

class Attribute implements Jsonable
{
    /**
     * @var string
     */
    private $key;
    /**
     * @var string
     */
    private $description;
    /**
     * @var string
     */
    private $value;

    /**
     * Attribute constructor.
     * @param string $key
     * @param string $description
     * @param string $value
     */
    public function __construct(string $key, string $description, string $value)
    {
        $this->key = $key;
        $this->description = $description;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    public function toJsonArray(): array
    {
        return [
            'key' => $this->getKey(),
            'description' => $this->getDescription(),
            'value' => $this->getValue(),
        ];
    }
}
