<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 5/9/20
 * Time: 10:28 PM
 */

namespace MiamiOH\LaravelStatusCheck\Resolvers;

interface ValueResolver
{
    public function getValue(): string;
}
