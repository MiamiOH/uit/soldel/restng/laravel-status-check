<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 5/9/20
 * Time: 9:23 PM
 */

namespace MiamiOH\LaravelStatusCheck\Resolvers;

interface ParamResolver
{
    public function getParams(): array;
}
