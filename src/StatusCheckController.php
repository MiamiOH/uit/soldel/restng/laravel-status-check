<?php
/**
 * Created by PhpStorm.
 * User: liaom
 * Date: 5/7/20
 * Time: 6:15 PM
 */

namespace MiamiOH\LaravelStatusCheck;

use Illuminate\Contracts\Container\Container;
use Illuminate\Http\Request;
use Subfission\Cas\CasManager;

class StatusCheckController
{
    /**
     * @var CheckStatus
     */
    private $checkStatus;
    /**
     * @var Container
     */
    private $container;
    /**
     * @var Authorization
     */
    private $authorization;

    /**
     * StatusCheckController constructor.
     * @param CheckStatus $checkStatus
     * @param Container $container
     * @param Authorization $authorization
     */
    public function __construct(CheckStatus $checkStatus, Container $container, Authorization $authorization)
    {
        $this->checkStatus = $checkStatus;
        $this->container = $container;
        $this->authorization = $authorization;
    }

    public function index()
    {
        $authConfig = config('status.authorization', []);
        if (!in_array('anonymous', $authConfig) && count($authConfig) !== 0) {
            /** @var CasManager $cas */
            $cas = app('cas');
            if (!$cas->isAuthenticated()) {
                $cas->authenticate();
            }
        }

        if (!$this->authorization->isAuthorized()) {
            return view('status::unauthorized');
        }

        return view('status::index', [
            'attributes' => $this->checkStatus->getAttributes()->toJsonArray(),
        ]);
    }

    public function getRequests(Request $request)
    {
        if (!$this->authorization->isAuthorized($request->get('token') ?? null)) {
            return response()->json('You are not authorized to get status requests.', 401);
        }

        return response()->json($this->checkStatus->getAllCheckStatusRequests()->map(function (
            CheckStatusRequest $request
        ) {
            $data = $request->toJsonArray();
            $name = $request->getCheckerName($this->container);
            $data['name'] = $name;

            return $data;
        })->toArray());
    }

    public function getStatus(Request $request)
    {
        if (!$this->authorization->isAuthorized($request->get('token') ?? null)) {
            return response()->json('You are not authorized to get status.', 401);
        }

        $rawRequest = $request->get('request') ?? null;
        $forceRefresh = $request->get('force-refresh', null) === 'true';

        if ($rawRequest !== null) {
            $request = CheckStatusRequest::createFromArray(json_decode($rawRequest, true));
            $status = $this->checkStatus->check($request, $forceRefresh);

            return response()->json($status->toJsonArray());
        }

        $requests = $this->checkStatus->getAllCheckStatusRequests();
        $statusCollection = $this->checkStatus->checkAll($requests, $forceRefresh);

        return response()->json([
            'isHealthy' => $statusCollection->isHealthy(),
            'attributes' => $this->checkStatus->getAttributes()->toJsonArray(),
            'status' => $statusCollection->toJsonArray(),
        ]);
    }
}
