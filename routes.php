<?php

Route::group(['middleware' => ['web']], function () {
    Route::group(['prefix' => config('status.urlPrefix', 'status')], function () {
        Route::get('/', 'MiamiOH\\LaravelStatusCheck\\StatusCheckController@index');
    });
});

Route::group(['middleware' => ['api'], 'prefix' => 'api'], function () {
    Route::group(['prefix' => config('status.urlPrefix', 'status')], function () {
        Route::get('/', 'MiamiOH\\LaravelStatusCheck\\StatusCheckController@getStatus')
            ->name('check-status');
        Route::get('/requests', 'MiamiOH\\LaravelStatusCheck\\StatusCheckController@getRequests')
            ->name('status-get-requests');
    });
});
